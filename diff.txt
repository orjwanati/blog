diff --git a/app/controllers/comments_controller.rb b/app/controllers/comments_controller.rb
index 9ff46c9..ef4324b 100644
--- a/app/controllers/comments_controller.rb
+++ b/app/controllers/comments_controller.rb
@@ -24,11 +24,12 @@ class CommentsController < ApplicationController
   # POST /comments
   # POST /comments.json
   def create
-    @comment = Comment.new(comment_params)
+    @post = Post.find(params[:post_id])
+    @comment = @post.comments.create(comment_params)
 
     respond_to do |format|
       if @comment.save
-        format.html { redirect_to @comment, notice: 'Comment was successfully created.' }
+        format.html { redirect_to @post, notice: 'Comment was successfully created.' }
         format.json { render :show, status: :created, location: @comment }
       else
         format.html { render :new }
diff --git a/app/controllers/posts_controller.rb b/app/controllers/posts_controller.rb
index 16bba99..16a3aef 100644
--- a/app/controllers/posts_controller.rb
+++ b/app/controllers/posts_controller.rb
@@ -1,5 +1,6 @@
 class PostsController < ApplicationController
   before_action :set_post, only: [:show, :edit, :update, :destroy]
+  before_action :authenticate, except: [:index, :show]
 
   # GET /posts
   # GET /posts.json
@@ -28,7 +29,7 @@ class PostsController < ApplicationController
 
     respond_to do |format|
       if @post.save
-        format.html { redirect_to @post, notice: 'Post was successfully created.' }
+        format.html { redirect_to post_url(@post), notice: 'Post was successfully created.' }
         format.json { render :show, status: :created, location: @post }
       else
         format.html { render :new }
@@ -71,4 +72,11 @@ class PostsController < ApplicationController
     def post_params
       params.require(:post).permit(:title, :body)
     end
-end
+
+    def authenticate 
+      authenticate_or_request_with_http_basic do |name, password|
+      name == "admin" && password == "secret"
+      end
+    end
+  end
+
diff --git a/app/views/posts/show.html.erb b/app/views/posts/show.html.erb
index c14ef10..a81177d 100644
--- a/app/views/posts/show.html.erb
+++ b/app/views/posts/show.html.erb
@@ -10,5 +10,27 @@
   <%= @post.body %>
 </p>
 
+<h2>Comments</h2>
+<div id="comments">
+	<%= @post.comments.each do |comment| %>
+	<%= div_for comment do %>
+	<p>
+		<strong>Posted <%= time_ago_in_words(comment.created_at) %></strong><br />
+		<%= h(comment.body) %>
+	</p>
+	<% end %>
+<% end %>
+</div>
+
+<%= form_for([@post, Comment.new]) do |f|  %>
+<p>
+	<%= f.label :body, "New Comment" %><br />
+	<%= f.text_area :body %>
+</p>
+<p>
+	<%= f.submit "Add Comment" %>
+</p>
+<% end %>
+
 <%= link_to 'Edit', edit_post_path(@post) %> |
 <%= link_to 'Back', posts_path %>
diff --git a/config/routes.rb b/config/routes.rb
index 890f377..f015e66 100644
--- a/config/routes.rb
+++ b/config/routes.rb
@@ -1,5 +1,5 @@
 Rails.application.routes.draw do
-  
+  #resources :comments
   resources :posts do
     resources :comments
   end
